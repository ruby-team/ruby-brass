---
source:
- meta
authors:
- name: Thomas Sawyer
  email: transfire@gmail.com
copyrights:
- holder: Rubyworks
  year: '2012'
  license: BSD-2-Clause
replacements: []
alternatives: []
requirements:
- name: detroit
  groups:
  - build
  development: true
- name: lemon
  groups:
  - test
  development: true
- name: rubytest
  groups:
  - test
  development: true
dependencies: []
conflicts: []
repositories:
- uri: git@github.com:rubyworks/brass.git
  scm: git
  name: upstream
resources:
  home: http://rubyworks.github.com/brass
  docs: http://rubydoc.info/gems/brass
  code: http://github.com/rubyworks/brass
  mail: http://groups.google.com/groups/rubyworks-mailinglist
extra: {}
load_path:
- lib
revision: 0
created: '2012-01-24'
summary: Bare-Metal Ruby Assertion System Standard
title: BRASS
version: 1.2.1
name: brass
description: ! 'BRASS stands for Bare-Metal Ruby Assertion System Standard. It is
  a very basic

  foundational assertions framework for other assertion and test frameworks

  to make use so they can all work together harmoniously.'
organization: Rubyworks
date: '2012-02-09'
